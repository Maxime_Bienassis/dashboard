import React, {  Component } from 'react';

import './admin.css';

const axios = require ('axios');


class admin extends Component {

  send = async (e) => {{
    if (this.state.value5 === 'Album'){
      axios.put(
    
      'http://localhost:3001/albums',
      
      {
        title: this.state.myInputValue1,
        release: this.state.myInputValue2,
        kind: this.state.myInputValue3 ,
        cover_url:this.state.myInputValue4
      })
    .then(function(response){
      console.log('album saved succesfully')
      
    });
    }
    else if(this.state.value5 === 'Artist'){
      axios.put(
    
      'http://localhost:3001/artists',
      
      {
        name: this.state.myInputValue1,
        birth: this.state.myInputValue2,
        followers: this.state.myInputValue3,
        albums: this.state.myInputValue4
      })
    .then(function(response){
      console.log('artist saved succesfully')
      
    });
    }
    else if(this.state.value5 ==='Track'){
      axios.put(
    
        'http://localhost:3001/tracks',
        
        {
          title: this.state.myInputValue1,
          duration: this.state.myInputValue2,
          listenings: this.state.myInputValue3,
          likes:this.state.myInputValue4
        })
      .then(function(response){
        console.log('track saved succesfully')
        
      });
    }
    
  
  }};
       
    constructor(props) {
      super(props);
      this.state={
        value1:'Name :',
        value2:'Birth :',
        value3:'Followers :',
        value4:'Album :',
        value5:'Artist'
      }
    }
  
  
  render() {
    return (
     
      <div className="profile "> 
          <h2>Ajout {this.state.value5}</h2>    
            <form className="text-center">
              <label>{this.state.value1} 
              <input
                  type="text"
                  defaultValue={this.state.myInputValue1}
                  onChange={e => this.setState({myInputValue1: e.target.value})} 
                  />
              </label>
                <br/>
                <label>{this.state.value2} 
                <input
                  type="text"
                  defaultValue={this.state.myInputValue2}
                  onChange={e => this.setState({myInputValue2: e.target.value})} 
                  />
              </label>
              <br/>
              <label>{this.state.value3} 
              <input
                  type="text"
                  defaultValue={this.state.myInputValue3}
                  onChange={e => this.setState({myInputValue3: e.target.value})} 
                  />
              </label>
              <br/>
              <label>{this.state.value4} 
              <input
                  type="text"
                  defaultValue={this.state.myInputValue4}
                  onChange={e => this.setState({myInputValue4: e.target.value})} 
                  />
                </label>
                <br/>
              
                
              <input 
              type="submit" 
              value="Submit" 
              onClick={this.send}/>
              
              <br/>
              <button
                type="button"
                id="bouton"
                onClick = {()=>{this.setState({value1:'Title : ',value2:'Duration : ',value3:'Listenings : ',value4:'Likes : ',value5:'Track'})}}
                >               

                  
              
        Add Track
        </button>
        <button
                type="button"
                id="bouton"
                onClick = {()=>{this.setState({value1:' Title : ',value2:'Release : ',value3:'Genre : ',value4:'Cover Url : ',value5:'Album :'})}}
              >
        Add Album
        </button>
        <button
                type="button"
                id="bouton"
                onClick = {()=>{this.setState({value1:' Name : ',value2:'Birth : ',value3:'Followers : ',value4:'Album : ',value5:'Artist'})}}
              >
        Add Artist
        </button>
            </form>
                       
        </div>
    );
  }
}


export default admin;
