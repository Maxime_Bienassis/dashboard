import React, { PureComponent } from 'react';
import {
  AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip,
} from 'recharts';


class LineC extends PureComponent{
    static jsfiddleUrl = 'https://jsfiddle.net/alidingling/xujpnxxp/';
    constructor(props){
        
        super(props);
        const axios = require ('axios');
        
        var fl= [];
        
        axios.get('http://localhost:3001/albums')
        .then (({ data }) =>{
           
            for(var i=0;i<data.length;i++){
             if (fl[data[i].release]===undefined){
                fl[data[i].release] = 1;
                
             }
            else{
            for(var j=1800;j<2019;j++)
            {

                if (j===data[i].release)
                {
                    fl[j]++;

                }
                
            }}
            
                
            }
        
        
        
        for(var k=1800;k<2019;k++){
            if (fl[k]!==undefined){
                this.setState(prevState => ({
                    data_chart: [...prevState.data_chart, {release: k, album: fl[k]}]
                    
                }))
                
            
        }
        }});        
                    
        this.state= {
            data_chart: []

           
            }}
                
            render() {
                return (
                  <AreaChart
                    width={500}
                    height={400}
                    data={this.state.data_chart}
                    margin={{
                      top: 10, right: 30, left: 0, bottom: 0,
                    }}
                  >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="release" />
                    <YAxis />
                    <Tooltip />
                    <Area type="monotone" dataKey="album" stroke="#8884d8" fill="#8884d8" fillOpacity={0.3} />
                    
                  </AreaChart>
                )
              }
}
export default LineC;