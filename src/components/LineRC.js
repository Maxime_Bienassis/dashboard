import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, Legend,
 } from 'recharts';
     
import React,{  PureComponent } from 'react';





class LineR extends PureComponent{
    constructor(props){
        super(props);
        const axios = require ('axios');
  axios.get('http://localhost:3001/tracks')
.then(({ data }) => {
  for(var i= 0; i <= 3;i++)
  {
    this.setState(prevState => ({
      data_chart: [...prevState.data_chart, {name: data[i].title, Likes: data[i].likes, electronicMusicListener: 4300, amt: 2100,}]
    }))
  }});
   /*const axios = require ('axios');
        var fl= [];
        var fa =[];
            axios.get('http://localhost:3001/artists')
            .then(({ data }) => {
            for(var i= 0; i <= 6;i++)
                {
                    fl[i]=data[i].followers;
                    fa[i]=data[i].name;
                    this.setState(prevState => ({
                    data_chart: [...prevState.data_chart, {name: '2006', Likes: data[i].followers, electronicMusicListener: 4300, amt: 2100,}]
                }))
                }});*/
        
        
        
        

        this.state= {  
          
        data_chart : [
  /*{
    
    name: '2000', Likes: 1000, electronicMusicListener: 2400, amt: 2400,
  },
  {
    name: '2001', Likes: 1000, electronicMusicListener: 1398, amt: 2210,
  },
  {
    name: '2002', Likes: 1000, electronicMusicListener: 9800, amt: 2290,
  },
  {
    name: '2003', Likes: 1000, electronicMusicListener: 3908, amt: 2000,
  },
  {
    name: '2004', Likes: 1000, electronicMusicListener: 4800, amt: 2181,
  },
  {
    name: '2005', Likes: 1000, electronicMusicListener: 3800, amt: 2500,
  },
  {
    name: '2006', Likes: 1000, electronicMusicListener: 4300, amt: 2100,
  },
*/]
    }
}


render(){
  
    return(
        <div className="chart">
            <ResponsiveContainer height={250} width='99%'>                         
                <LineChart
                    data={this.state.data_chart}
                    margin={{top: 20, right: 20, left: 20, bottom: 20,}}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line type="monotone" dataKey="Likes" stroke="#8884d8" />
                   
                </LineChart>   
            </ResponsiveContainer>
        </div>
       )  
    }
}

export default LineR;      
      
      
     