import React, { Component } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
const axios = require ('axios');


class BarC extends Component{
    static jsfiddleUrl = 'https://jsfiddle.net/alidingling/30763kr7/';
    constructor(props){
        super(props);
        
        var fl= [];
        var fa =[];
            axios.get('http://localhost:3001/artists')
            .then(({ data }) => {
            for(var i= 0; i <= data.length-1 ;i++)
                {
                  
                    fl[i]=data[i].followers;
                    fa[i]=data[i].name;
                    this.setState(prevState => ({
                    data_chart: [...prevState.data_chart, {name: fa[i], fans: fl[i]}]
                    
                }))
                }});
        this.state= {  
        
        data_chart : []
        
        }
    }
          
        
          render() {
            return (
              <BarChart
                width={500}
                height={300}
                data={this.state.data_chart}
                margin={{
                  top: 5, right: 30, left: 20, bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="fans" stackId ="a" fill="#8884d8" />
                <Bar dataKey="album" stackId ="a" fill="#0884d8" />
              </BarChart>
            );
          }
        }
    
    
    
        
    
export default BarC;